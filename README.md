# Self Driving RC Car

This project holds all of our technical documents. There is no code stored here.

Google Drive: [link](https://drive.google.com/drive/u/1/folders/1xrFQLmfmEXoz_LBpqhL12_4TH2SL0CTI)


### Structure
```
.
├── 100-Design_Documents            # Documents about the overall design of the project
├── 200-Datasheets_&_Manuals        # Where all of the datasheets for the circuits and sensors we use
├── 300-Schematics                  # Our own electronic schematics
└── 400-Internal_Documents          # Miscellaneous documentation and tutorials
```

